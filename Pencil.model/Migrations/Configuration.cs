namespace Pencil.model.Migrations
{
    using Identity;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Pencil.model.Context.PencilContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Pencil.model.Context.PencilContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //



            //context.Roles.AddOrUpdate(m => m.Id,
            //    new Microsoft.AspNet.Identity.EntityFramework.IdentityRole() { Id = Guid.NewGuid().ToString(), Name = "Administrator" },
            //    new Microsoft.AspNet.Identity.EntityFramework.IdentityRole() { Id = Guid.NewGuid().ToString(), Name = "Translator" },
            //    new Microsoft.AspNet.Identity.EntityFramework.IdentityRole() { Id = Guid.NewGuid().ToString(), Name = "User" }
            //    );
        }
    }
}
