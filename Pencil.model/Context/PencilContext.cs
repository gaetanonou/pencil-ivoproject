﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Pencil.model.Identity;

namespace Pencil.model.Context
{
   
    public class PencilContext : IdentityDbContext<ApplicationUser>
    {
        public PencilContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }


        // App Models

        public static PencilContext Create()
        {
            return new PencilContext();
        }
    }

}
