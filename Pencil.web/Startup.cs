﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pencil.web.Startup))]
namespace Pencil.web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
